package Popeiu.Roxana.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class View extends javax.swing.JFrame {



    private JButton writeButton;
    private JScrollPane jScrollPane1;
    private JTextArea textArea;
    private JTextField textField;
    public View() {
        initComponents();
    }


    private void initComponents() {

        textField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        writeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        textField.setText("123456");

        textArea.setColumns(20);
        textArea.setRows(5);
        jScrollPane1.setViewportView(textArea);

        writeButton.setText("Write");


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(50, 50, 50)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(textField)
                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(155, 155, 155)
                                                .addComponent(writeButton)))
                                .addContainerGap(78, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addComponent(textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                                .addComponent(writeButton)
                                .addGap(26, 26, 26))
        );

        pack();
    }


    public JButton getWriteButton() {
        return writeButton;
    }

    public void setWriteButton(JButton writeButton) {
        this.writeButton = writeButton;
    }

    public JScrollPane getjScrollPane1() {
        return jScrollPane1;
    }

    public void setjScrollPane1(JScrollPane jScrollPane1) {
        this.jScrollPane1 = jScrollPane1;
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(String str) {
        this.textArea.setText(str);
    }

    public String getTextField() {
        return textField.getText();
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }
}

class Controller implements ActionListener{

    private View view;

    public Controller(){
        view=new View();
        view.getWriteButton().addActionListener(this::actionPerformed);
        view.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource()==view.getWriteButton()){
            int n=view.getTextField().length();
            dislayA(n);
        }
    }

    public void dislayA(int n){
        StringBuilder stringBuilder=new StringBuilder("");
        for(int i=0;i<n;i++){
            stringBuilder.append("a");
        }
        view.setTextArea(stringBuilder.toString());

    }
}

public class Ex2 {

    public static void main(String[] args){
        Controller controller=new Controller();
    }
}
